# hwloc extra tests

Contains tests that the CI will run but we won't include
in the main repo or tarball, e.g. tests that are huge,
long, ...

Each subdirectory should be symlink in the corresponding
subdir in a hwloc build tree.
For instance tests/hwloc/xml/extra should be a symlink
to here/xml.

Each subdirectory may contain files existing as stored
in the corresponding hwloc tests subdir.

# How to use

To create all symlinks from a hwloc build tree to here:
$ setup-links.sh /path/to/hwloc/build/tree
This will abort if some symlinks already exists.

To force remove symlinks and create new ones:
$ setup-links.sh -f /path/to/hwloc/build/tree

To only remove existing symlinks without creating new ones:
$ setup-links.sh -r /path/to/hwloc/build/tree
