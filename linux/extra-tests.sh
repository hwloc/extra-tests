#!/bin/bash

LOG_COMPILER="$1"

if test -z "$LOG_COMPILER" -o ! -e "$LOG_COMPILER"; then
    echo "LOG_COMPILER program needed as argument"
    exit 1
fi

cd $(dirname "$0")

echo "============================================================================"
echo "Running extra tests under $PWD"

total=0
passed=0
failed=0
for file in $(ls *.test); do
    name=$(basename "$file" .test)
    if "$LOG_COMPILER" $file > $name.log; then
	echo "PASS: $name"
	passed=$((passed+1))
    else
	echo "FAIL: $name"
	failed=$((failed+1))
    fi
    total=$((total+1))
done
echo "============================================================================"
echo "# TOTAL: $total"
echo "# PASS:  $passed"
echo "# FAIL:  $failed"
echo "============================================================================"

exit $(test $failed -eq 0)
