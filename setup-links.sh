#!/bin/sh

cd `dirname $0`

usage() {
    echo "$0 [-f --force -r --remove] [hwloc build tree]"
}

force=0
remove=0
while [ $# -gt 0 ]; do
    case x$1 in
	x-f|x--force) force=1; shift;;
	x-r|x--remove) remove=1; shift;;
	x-*) echo "Unrecognized options: $1"; usage; exit 1;;
	*) break
    esac
done

hwloctree="$1"
if ! test -d "$hwloctree"; then
    echo "Missing hwloc tree as argument"
    usage
    exit 1
fi
if ! test -x "$hwloctree/config.status"; then
    echo "Doesn't look like a hwloc build tree (no config.status)"
    usage
    exit 1
fi

SUBDIRS="xml x86 x86+linux linux linux/allowed"

# look for existing links if not -f and not -r
if test x$force = x0 -a x$remove = x0; then
    for subdir in $SUBDIRS; do
	if test -e "$hwloctree/tests/hwloc/$subdir/extra" -o -L "$hwloctree/tests/hwloc/$subdir/extra"; then
	    echo "Found existing extra child $hwloctree/tests/hwloc/$subdir/extra, aborting."
	    exit 1;
	fi
    done
fi

# remove existing links if -f or -r
if test x$force = x1 -o x$remove = x1; then
    for subdir in $SUBDIRS; do
	if test -e "$hwloctree/tests/hwloc/$subdir/extra" -o -L "$hwloctree/tests/hwloc/$subdir/extra"; then
	    echo "Removing $hwloctree/tests/hwloc/$subdir/extra"
	    if ! rm -f "$hwloctree/tests/hwloc/$subdir/extra" ; then
		echo "Failed, aborting. File was:"
                ls -ld "$hwloctree/tests/hwloc/$subdir/extra"
		exit 1;
	    fi
	fi
    done
fi

# exit if -r only
test x$remove = x1 && exit 0

# now symlink for real
for subdir in $SUBDIRS; do
    target=`echo $subdir | tr / -`
    echo "Symlinking $hwloctree/tests/hwloc/$subdir/extra to $PWD/$target"
    ln -s "$PWD/$target" "$hwloctree/tests/hwloc/$subdir/extra"
done
